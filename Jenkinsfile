pipeline {
    agent any
    environment {
        DOCKER_REPO = '40.69.134.145:8381'
        DOCKER_CREDENTIALS = 'nexus-credentials'
    }
    stages {
        stage('Build and Test artifact') {
            agent {
                docker {
                    image 'maven:3.6.3-jdk-11'
                    reuseNode true
                }
            }
            steps {
                sh 'mvn clean package -B -ntp'
            }
            post {
                success {
                    jacoco()
                }
            }
        }

        /*
        Configurate SonarQube webhook for quality gate
        Administration > Configuration > Webhooks > Create
        The URL should point to your Jenkins server http://{JENKINS_HOST}/sonarqube-webhook/
        */
        stage('Sonarqube analysis') {
            agent {
                docker {
                    image 'maven:3.6.3-jdk-11'
                    reuseNode true
                }
            }
            steps {
                withSonarQubeEnv('sonarqube-local') {
                    sh 'mvn sonar:sonar -B -ntp'
                }
                timeout(time: 10, unit: 'MINUTES') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }

        stage('Deploy Nexus') {
            agent {
                docker {
                    image 'maven:3.6.3-jdk-11'
                    reuseNode true
                }
            }
            steps {
                script {
                    pom = readMavenPom file: 'pom.xml'
                    nexusPublisher nexusInstanceId: 'nexus-local',
                    nexusRepositoryId: 'maven-releases',
                    packages: [[$class: 'MavenPackage',
                    mavenAssetList: [[
                        classifier: '',
                        extension: '',
                        filePath: "target/${pom.artifactId}-${pom.version}.${pom.packaging}"]],
                    mavenCoordinate: [
                        artifactId: "${pom.artifactId}",
                        groupId: "${pom.groupId}",
                        packaging:"${pom.packaging}",
                        version: "${pom.version}"]
                        ]]
                }
            }
        }
        stage('Build and Deploy to Nexus Registry') {
            steps {
                script {
                    timeout(time: 5, unit: 'MINUTES') {
                        userInput = input(
                           submitterParameter: 'approval',
                           message: '¿Desea desplegar en Docker el artefacto?', parameters: [
                           [$class: 'BooleanParameterDefinition', defaultValue: true, description: '', name: 'Aprobar']
                       ])
                    }

                    if (userInput.Aprobar == true) {
                        pom = readMavenPom file: 'pom.xml'
                        dockerImage = docker.build("${DOCKER_REPO}/${pom.artifactId}:${pom.version}")

                        withDockerRegistry([credentialsId: "$DOCKER_CREDENTIALS", url: "http://$DOCKER_REPO"]) {
                            dockerImage.push()
                            dockerImage.push('latest')
                        }
                    }
                }
            }
        }
    }
    post {
        always {
            deleteDir()
        }
    }
}
